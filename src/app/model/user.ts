export interface User {

  username:string;
  password:string;
  weight:number;
  age : number;
  height : number;

}
