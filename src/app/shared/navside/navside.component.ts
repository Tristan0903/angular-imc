import { Component, OnInit } from '@angular/core';
import { ConnexionComponent } from 'src/app/component/connexion/connexion.component';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-navside',
  templateUrl: './navside.component.html',
  styleUrls: ['./navside.component.css']
})
export class NavsideComponent implements OnInit {
  username : string = "Vincent"
  constructor() { }

  ngOnInit(): void {

  }

}
