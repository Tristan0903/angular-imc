import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueuilComponent } from './component/accueuil/accueuil.component';
import { ConnexionComponent } from './component/connexion/connexion.component';
import { ModifpoidsComponent } from './component/modifpoids/modifpoids.component';
import { PageutilisateurComponent } from './component/pageutilisateur/pageutilisateur.component';

const routes: Routes = [
    {path : '', component: AccueuilComponent },
    {path : '',
    children: [
      {path : 'component/connexion', component : ConnexionComponent},
      {path : 'component/accueuil', component : AccueuilComponent},
      {path : 'component/pageutilisateur', component : PageutilisateurComponent},
      {path : 'component/modifpoids', component : ModifpoidsComponent}
    ]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
