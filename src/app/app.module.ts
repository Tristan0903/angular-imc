import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms'
import {ReactiveFormsModule} from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccueuilComponent } from './component/accueuil/accueuil.component';
import { HeaderComponent } from './shared/header/header.component';
import { ConnexionComponent } from './component/connexion/connexion.component';
import { PageutilisateurComponent } from './component/pageutilisateur/pageutilisateur.component';
import { NavsideComponent } from './shared/navside/navside.component';
import { ModifpoidsComponent } from './component/modifpoids/modifpoids.component';
import { registerLocaleData } from '@angular/common';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import en from '@angular/common/locales/en';
import { NZ_ICONS } from 'ng-zorro-antd/icon';
import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';
import { IconDefinition } from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';
import { IMCSemaineComponent } from './component/pageutilisateur/imcsemaine/imcsemaine.component';
import { IMCMoisComponent } from './component/pageutilisateur/imcmois/imcmois.component';
import { IMCTrimestreComponent } from './component/pageutilisateur/imctrimestre/imctrimestre.component';



@NgModule({
  declarations: [
    AppComponent,
    AccueuilComponent,
    HeaderComponent,
    ConnexionComponent,
    PageutilisateurComponent,
    NavsideComponent,
    ModifpoidsComponent,
    IMCSemaineComponent,
    IMCMoisComponent,
    IMCTrimestreComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
