import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IMCSemaineComponent } from './imcsemaine.component';

describe('IMCSemaineComponent', () => {
  let component: IMCSemaineComponent;
  let fixture: ComponentFixture<IMCSemaineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IMCSemaineComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IMCSemaineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
