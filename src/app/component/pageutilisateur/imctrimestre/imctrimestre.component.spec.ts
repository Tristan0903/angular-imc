import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IMCTrimestreComponent } from './imctrimestre.component';

describe('IMCTrimestreComponent', () => {
  let component: IMCTrimestreComponent;
  let fixture: ComponentFixture<IMCTrimestreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IMCTrimestreComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IMCTrimestreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
