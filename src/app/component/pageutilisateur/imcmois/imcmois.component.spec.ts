import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IMCMoisComponent } from './imcmois.component';

describe('IMCMoisComponent', () => {
  let component: IMCMoisComponent;
  let fixture: ComponentFixture<IMCMoisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IMCMoisComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IMCMoisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
