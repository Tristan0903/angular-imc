import { Component, OnInit } from '@angular/core';
import { HeaderService } from 'src/app/services/header.service';

@Component({
  selector: 'app-pageutilisateur',
  templateUrl: './pageutilisateur.component.html',
  styleUrls: ['./pageutilisateur.component.css']
})
export class PageutilisateurComponent implements OnInit {
titre : string = "Angular IMC"

  constructor(private headerService : HeaderService) { }

  ngOnInit(): void {
    this.headerService.changeTitre(this.titre);
  }

}
