import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { User } from 'src/app/model/user';
import { HeaderService } from 'src/app/services/header.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {
  titre : string = "Accueil";
  myGroup !: FormGroup;
  user! : User
  constructor(private headerService : HeaderService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.headerService.changeTitre(this.titre);
    this.myGroup = this.fb.group({
      username: this.fb.control('', Validators.required),
      password : this.fb.control('', [Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")]),


    });
  }

   get username(){
    return this.myGroup.get('username');

  }


  get password(){
    return this.myGroup.get("password")
    console.log();

  }

  validation() {
    if(this.myGroup.valid){
      this.user = this.myGroup.value;

    }
  }

}






