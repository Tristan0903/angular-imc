import { Component, OnInit } from '@angular/core';
import { HeaderService } from 'src/app/services/header.service';


@Component({
  selector: 'app-modifpoids',
  templateUrl: './modifpoids.component.html',
  styleUrls: ['./modifpoids.component.css']
})
export class ModifpoidsComponent implements OnInit {

  titre : string = "Saisir votre poids"
  constructor(private headerServcice : HeaderService) { }


  ngOnInit(): void {
    this.headerServcice.changeTitre(this.titre);
  }

}
