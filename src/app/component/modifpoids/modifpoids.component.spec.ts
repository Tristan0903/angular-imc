import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifpoidsComponent } from './modifpoids.component';

describe('ModifpoidsComponent', () => {
  let component: ModifpoidsComponent;
  let fixture: ComponentFixture<ModifpoidsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifpoidsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModifpoidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
