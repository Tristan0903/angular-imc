import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/model/user';
import { HeaderService } from 'src/app/services/header.service';

@Component({
  selector: 'app-accueuil',
  templateUrl: './accueuil.component.html',
  styleUrls: ['./accueuil.component.css']
})
export class AccueuilComponent implements OnInit {
 titre : string = "Accueil";
 myGroup !: FormGroup;
 user! : User
  constructor(private headerService : HeaderService, private fb : FormBuilder) { }

  ngOnInit(): void {
    this.headerService.changeTitre(this.titre);
    this.myGroup = this.fb.group({
      username: this.fb.control('', Validators.required),
      password : this.fb.control('', [Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")]),
      age : this.fb.control('', [Validators.required, Validators.pattern("^(0?[1-9]|[1-9][0-9]|[1][1-9][1-9]|500)$")]),
      weight : this.fb.control('', Validators.required),
      height : this.fb.control('', Validators.required)


    });

  }

  get username(){
    return this.myGroup.get('username');
    console.log('username');
  }


  get password(){
    return this.myGroup.get('password')
  }

  get age(){
    return this.myGroup.get('age')
  }

  get weight(){
    return this.myGroup.get('weight')
  }

  get height(){
    return this.myGroup.get('height')
  }


  validation() {
    if(this.myGroup.valid){
      this.user = this.myGroup.value
    }
  }


}
